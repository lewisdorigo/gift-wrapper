# Gift Wrapper

A simple plugin to add gift wrapping options to WooCommerce.

## Installation

Simply add the plugin to your `plugins` directory, and activate in the WordPress admin.

### Using Composer

Add the repository to your composer.json:

    {
      "repositories": [
        {
          "type": "composer",
          "url": "https://wpackagist.org"
        }
      ]
    }

Then run `composer require dorigo/wc-gift-wrap`, and activate the plugin in the WordPress admin.