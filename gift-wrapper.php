<?php
/*
Plugin Name: Gift Wrapper
Plugin URI: https://bitbucket.org/lewisdorigo/gift-wrapper
Description: A simple plugin to add gift wrapping options to WooCommerce
Version: 1.0.0
Author: Lewis Dorigo
Author URI: https://dorigo.co
*/

namespace Dorigo;

class ProductGiftWrap {
    private $default_message = '{checkbox} Gift wrap this item for {price}?';

    private $gift_wrap_enabled = false;
    private $gift_wrap_price   = 0;
    private $gift_wrap_message = '';

    private $settings = [
        [
            'name' => 'Gift Wrapping Enabled by Default',
            'desc' => 'Allow gift wrapping for all products by default?',
            'id'   => 'gift_wrap_enabled',
            'type' => 'checkbox',
        ],
        [
            'name' => 'Gift Wrap Price',
            'desc' => 'The price of gift wrapping. Can be overwritten per-product.',
            'id'   => 'gift_wrap_price',
            'type' => 'text',
            'desc_tip' => true,
        ],
        [
            'name' => 'Gift Wrap Text',
            'desc' => '<code>{checkbox}</code> and <code>{price}</code> will be replaced with the checkbox and cost.',
            'id'   => 'gift_wrap_message',
            'type' => 'text',
        ]
    ];

    public function __construct() {
        $this->gift_wrap_enabled = get_option('gift_wrap_enabled') === 'yes' ? true : false;
        $this->gift_wrap_price   = (float) get_option('gift_wrap_price', 0);
        $this->gift_wrap_message = get_option('gift_wrap_message') ?: $this->default_message;

        add_option('gift_wrap_enabled', 'false');
        add_option('gift_wrap_price', '0');
        add_option('gift_wrap_message', $this->default_message);

        $this->addFilters();
    }

    private function addFilters() {
        // Display on the front end
		add_action('woocommerce_after_add_to_cart_button', [$this, 'option_html'], 10);

		// Filters for cart actions
		add_filter('woocommerce_add_cart_item_data', [$this, 'add_cart_item_data'], 10, 2);
		add_filter('woocommerce_get_cart_item_from_session', [$this, 'get_cart_item_from_session'], 10, 2);
		add_filter('woocommerce_get_item_data', [$this, 'get_item_data'], 10, 2);
		add_filter('woocommerce_add_cart_item', [$this, 'add_cart_item'], 10, 1);
		add_action('woocommerce_add_order_item_meta', [$this, 'add_order_item_meta'], 10, 2);

		// Write Panels
		add_action('woocommerce_product_options_pricing', [$this, 'write_panel']);
		add_action('woocommerce_process_product_meta', [$this, 'write_panel_save']);

		// Admin
		add_action('woocommerce_settings_general_options_end', [$this, 'admin_settings']);
		add_action('woocommerce_update_options_general', [$this, 'save_admin_settings']);
    }

    public function option_html() {
        global $post;

        $wrappable = get_post_meta($post->ID, '_is_wrappable', true);
        $wrappable = $wrappable === '' && $this->gift_wrap_enabled ? 'yes' : $wrappable;

        if($wrappable) {
            $current_value = !empty($_REQUEST['gift_wrap']);

            $price = (float) get_post_meta($post->ID, '_gift_wrap_price', true);

            if(!$price) {
                $price = $this->gift_wrap_price;
            }

            $price_text = $price > 0 ? wc_price($price) : 'free';
            $checkbox   = '<input type="checkbox" name="gift_wrap" value="yes" '.checked($current_value, 1, false).'>';

            wc_get_template('gift-wrap.php', [
                'message'    => $this->gift_wrap_message,
                'checkbox'   => $checkbox,
                'price_text' => $price_text,
            ], 'gift-wrapper', untrailingslashit(plugin_dir_path(__FILE__)).'/templates/');
        }
    }

	private function cart_item_wrapping_price($cart_item) {
		$price = (float) get_post_meta($cart_item['product_id'], '_gift_wrap_price', true);
		$product_price = $cart_item['data']->get_price();

		$price = $price ?: $this->gift_wrap_price;

		$cart_item['data']->set_price($product_price + $price);

    	return $cart_item;
	}

    public function add_cart_item_data($cart_item_meta, $product_id) {
        $wrappable = get_post_meta($product_id, '_is_wrappable', true);
        $wrappable = $wrappable === '' && $this->gift_wrap_enabled ? 'yes' : $wrappable;

        if(!empty($_POST['gift_wrap']) && $wrappable) {
            $cart_item_meta['gift_wrap'] = true;
        }

        return $cart_item_meta;
    }

	public function get_cart_item_from_session($cart_item, $values) {
		if (empty($values['gift_wrap'])) { return $cart_item; }

		$cart_item['gift_wrap'] = true;

		return $this->cart_item_wrapping_price($cart_item);
	}

	public function get_item_data($item_data, $cart_item) {
    	if(empty($cart_item['gift_wrap'])) { return $item_data; }

    	$item_data[] = [
        	'name' => 'Gift Wrapped',
        	'value' => 'yes',
        	'display' => 'Yes',
    	];

    	return $item_data;
	}

	public function add_cart_item($cart_item) {
    	if(empty($cart_item['gift_wrap'])) { return $cart_item; }

    	return $this->cart_item_wrapping_price($cart_item);
	}

	public function add_order_item_meta($item_id, $cart_item) {
    	if(empty($cart_item['gift_wrap'])) { return; }

    	woocommerce_add_order_item_meta($item_id, 'Gift Wrapped', 'Yes');
	}

	public function write_panel() {
    	global $post;

		echo '</div><div class="options_group show_if_simple show_if_variable">';

		$wrappable = get_post_meta($post->ID, '_is_wrappable', true);
        $wrappable = $wrappable === '' && $this->gift_wrap_enabled ? 'yes' : $wrappable;

        woocommerce_wp_checkbox([
           'id'            => '_is_wrappable',
           'wrapper_class' => '',
           'value'         => $wrappable,
           'label'         => 'Wrappable',
           'description'   => 'Enable gift wrapping on this product?'
        ]);

        woocommerce_wp_text_input([
           'id'            => '_gift_wrap_price',
           'label'         => 'Price',
           'placeholder'   => $this->gift_wrap_price,
           'desc_type'     => true,
           'description'   => 'Override the default price?'
        ]);

        wc_enqueue_js("
            jQuery('input#_is_wrappable').on('change', function() {
                jQuery('._gift_wrap_price_field').hide();

                if(jQuery('#_is_wrappable').is(':checked') ) {
					jQuery('._gift_wrap_price_field').show();
				}
            }).change();
        ");
	}

	public function write_panel_save($post_id) {
		$wrappable = !empty($_POST['_is_wrappable']) ? 'yes' : 'no';
		$price     = !empty($_POST['_gift_wrap_price']) ? woocommerce_clean($_POST['_gift_wrap_price']) : '';

		update_post_meta($post_id, '_is_wrappable', $wrappable);
		update_post_meta($post_id, '_gift_wrap_price', $price);
	}

	public function admin_settings() {
    	woocommerce_admin_fields($this->settings);
	}

	public function save_admin_settings() {
    	woocommerce_update_options($this->settings);
	}
}

new ProductGiftWrap();